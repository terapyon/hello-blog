/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it
// import Parser from 'rss-parser'
Parser = require('rss-parser');
// import omitBy from 'lodash/omitBy'
const path = require("path");
const crypto = require('crypto');

// const normalize = (item) => {
//     const namespaceMatched = Object.keys(item).filter(e => e.match(/:/))
//     if (namespaceMatched.length === 0) {
//         return item
//     }

//     let namespaced = {}
//     namespaceMatched.forEach(key => {
//         const [namespace, childKey] = key.split(":")
//         if (!namespaced[namespace]) {
//             namespaced[namespace] = {}
//         }
//         namespaced[namespace][childKey] = item[key]
//     })

//     return {
//         ...omitBy(item, (_, key) => key.match(/:/)),
//         ...namespaced,
//     }
// }
// const renameSymbolMap = {
//     _: 'text',
//     $: 'attrs',
// }

// const renameSymbolKeys = (obj) => {
//     Object.keys(obj).forEach(key => {
//         if (typeof obj[key] === 'object') {
//             renameSymbolKeys(obj[key])
//         }
//         if (renameSymbolMap[key]) {
//             obj[renameSymbolMap[key]] = obj[key]
//             delete obj[key]
//         }
//     })
// }

exports.sourceNodes = async ({ actions }) => {
    const { createNode } = actions;
    const url = "https://www.cmscom.jp/blog/rss.xml"
    const parser = new Parser()
    const feed = await parser.parseURL(url)
    const { items, ...other } = feed
    // console.log(actions)
    items.forEach(item =>
        createNode({
            id: item.guid,
            date: item.pubDate,
            title: item.title,
            body: item.content,
            internal: {
                type: "blog",
                contentDigest: crypto
                    .createHash(`md5`)
                    .update(JSON.stringify(item))
                    .digest(`hex`),
                mediaType: "text/html"
            }
        })
    );
    return;
}

// const blogs = [
//     {
//         id: "1",
//         path: "blog/1",
//         date: "2019/02/15",
//         title: "My first blog post",
//         body: "<div>test 1 <br /><span>aaaa</span></div>"
//     },
//     {
//         id: "2",
//         path: "blog/2",
//         date: "2019/02/16",
//         title: "My second blog post",
//         body: "<div>test 2 <span>bbbbb</span></div>"
//     }
// ];



// exports.sourceNodes = async ({ actions }) => {
//     const { createNode } = actions;
//     // console.log(actions)
//     blogs.forEach(blog =>
//         createNode({
//             id: blog.id,
//             date: blog.date,
//             title: blog.title,
//             body: blog.body,
//             internal: {
//                 type: "blog",
//                 contentDigest: crypto
//                     .createHash(`md5`)
//                     .update(JSON.stringify(blog))
//                     .digest(`hex`),
//                 mediaType: "text/html"
//             }
//         })
//     );

//     return;
// }


// const { createFilePath } = require(`gatsby-source-filesystem`)
exports.onCreateNode = ({ node, getNode, actions }) => {
    // console.log(node.internal.type)
    const { createNodeField } = actions
    if (node.internal.type === `blog`) {
        // const fileNode = getNode(node.parent)
        // console.log(`\n`, fileNode.relativePath)
        // const slug = createFilePath({ node, getNode, basePath: `pages` })

        createNodeField({
            node,
            name: `slug`,
            value: '/blog/' + node.id,
        })
    }
}


exports.createPages = ({ graphql, actions }) => {
    const { createPage } = actions;
    return new Promise((resolve, reject) => {
        graphql(`
          {
            allBlog {
                edges {
                  node {
                    fields {
                      slug
                    }
                  }
                }
              }
          }
        `).then(result => {
            result.data.allBlog.edges.forEach(({ node }) => {
                createPage({
                    path: node.fields.slug,
                    component: path.resolve(`./src/templates/blog-template.js`),
                    context: {
                        slug: node.fields.slug,
                    },
                })
            }
            )
            resolve()
        })
    })
}
