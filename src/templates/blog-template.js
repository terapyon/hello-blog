// src/templates/blog-template.js
import React from 'react'
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { graphql } from "gatsby"

// const BlogTemplate = ({ pageContext: { title, path, date } }) => (
//     <Layout>
//         <SEO title="Abount" />
//         <h1>{`${title} Page`}</h1>
//         <div>date : {date}</div>
//         <Link to="/">Go to "Home"</Link>
//     </Layout>

// )

const BlogTemplate = ({ data }) => {
    const {
        blog: {
            title,
            date,
            body
        }
    } = data
    return (
        <Layout>
            <SEO title={title} />
            <h1>{`${title} Page`}</h1>
            <div>date : {date}</div>
            <div dangerouslySetInnerHTML={{ __html: body }} />
            <Link to="/blog">Go to "Blog"</Link>
        </Layout>
    )
}
export default BlogTemplate

export const pageQuery = graphql`
  query($slug: String!) {
    blog(fields: {slug: { eq: $slug }}) {
        title
        date
        body
      }
}
`