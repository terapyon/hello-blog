// src/pages/about.js

import React from 'react'
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { graphql } from "gatsby"

const About = ({ data }) => (
    <Layout>
        <SEO title="Abount" />
        <h1>About {data.site.siteMetadata.title}</h1>
        <p>これはAboutページ.</p>
        <Link to="/">Go to "Home"</Link>
    </Layout>
)

export default About


export const query = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`