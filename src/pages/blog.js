import React from 'react'
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const Blog = ({ data }) => {
  return (
    <Layout>
      <SEO title="Blog post list" />
      <h1>Blog post list</h1>
      {data.allBlog.edges.map(({ node }) => (
        <div key={node.id}>
          <Link to={node.fields.slug}>
            <h3>
              {node.title}{" "}
              <span>
                {"- "}{node.date}
              </span>
            </h3>
          </Link>
        </div>
      ))}

      <Link to="/blog">Go to "Blog Home"</Link>
    </Layout>
  )
}
export default Blog


export const query = graphql`
query {
    allBlog {
        edges {
          node {
            id
            date
            title
            fields {
              slug
            }
          }
        }
      }
  }
`